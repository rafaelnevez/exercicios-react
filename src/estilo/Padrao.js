import {StyleSheet} from 'react-native'

export default StyleSheet.create({
    ex:{
        paddingHorizontal:15,
        marginVertical:5,
        borderRadius:10,
        borderWidth:2,
        borderColor:'#222',
        fontSize:24,
        fontWeight:'bold'
    },
    impar:{
        color:'red'
    },
    par:{
        color:'blue'
    },
    input:{
        height: 70,
        fontSize: 40,
        borderColor: 'gray',
        borderWidth: 1,
        paddingHorizontal:30,
        marginVertical:25,
        marginHorizontal:25

    },
    fonte40:{
        marginVertical:25,
        marginHorizontal:25,
        fontSize: 40,
    }
})