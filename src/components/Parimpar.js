import React from 'react'
import { Text, View } from 'react-native'
import Padrao from '../estilo/Padrao'
import If from './If'
function parOuImpar(num) {
    if (num % 2 == 0) {
        return <Text style={[Padrao.ex, Padrao.par]}>Par {num}</Text>
    } else {
        return <Text style={[Padrao.ex, Padrao.impar]}>Impar {num}</Text>
    }
    /*     const v = num % 2 == 0 ? 'Par' : 'Impar'
        return <Text style={[Padrao.ex]}> {v} {num}</Text> */
}




/*{parOuImpar(props.numero)}

{         {
    props.numero % 2 == 0
    ? <Text style={[Padrao.ex, Padrao.par]}>Par {props.numero}</Text>
    : <Text style={[Padrao.ex,Padrao.impar]}>Impar {props.numero}</Text>
} }*/

export default props =>
    <View>
        <If test={props.numero % 2 == 0}>
            <Text style={[Padrao.ex, Padrao.par]}>Par {props.numero}</Text>
        </If>
        <If test={props.numero % 2 == 1}>
            <Text style={[Padrao.ex, Padrao.impar]}>Impar {props.numero}</Text>
        </If>
    </View>