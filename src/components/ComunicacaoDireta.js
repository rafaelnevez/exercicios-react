import React, { Component } from 'react'
import { View, Text } from 'react-native'
//import Padrao from '../estilo/Padrao'

const fonte = { style: { fontSize: 30, color:"red" } }
const fontePai = { style: { fontSize: 30, color:"blue" } }
const fonteFilho = { style: { fontSize: 30, color:"green" } }


function filhosComProps (props){
    return  React.Children.map(props.children,
        c => React.cloneElement(c, {...props,...c.props}))
}

export const Filho = props =>
    <View>
        <Text {...fonteFilho} >Filho: {props.nome} {props.sobrenome}</Text>
    </View>

export const Pai = props =>
    <View>
        <Text {...fontePai}>Pai: {props.nome} {props.sobrenome}</Text>
        {/*  {props.children}  */}
      {/*    {React.cloneElement(props.children, {...props, ...props.children.props})} {/* cloneElement so funcionan com 1 elemento */}
      {filhosComProps(props)}
    
    </View>
//É necessário dizer ao pai para redenrizar os filhos, pois no html isso é necessário.

export const Avo = props =>
    <View>
        <Text {...fonte}>Avó: {props.nome} {props.sobrenome}</Text>
        <Pai nome="André" sobrenome={props.sobrenome}>
            <Filho nome="Ana" />
            <Filho nome="Gui" />
            <Filho nome="Davi" /> 
        </Pai>
        <Pai {...props} nome="Pedro"> 
        {/* Nesse Caso o props equivale a todos anteriormente escritos (do Avô), podendo da override se declarar depois */}
            <Filho nome="Rebeca" />
            <Filho nome="Renato" /> 
        </Pai>

    </View>

    
export default Avo