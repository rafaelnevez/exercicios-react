import React from 'react'
import { StyleSheet, View, Image,TextInput } from 'react-native'


const styles = StyleSheet.create({
    container: {
        height: '100%'
    },
    emcima: {
        flexDirection: 'row',
        flex: 2
    },
    baixo: {
        flexDirection: 'row',
        flex: 2

    },
    meio: {
        flexDirection: 'row',
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
    },
    setor1: {
        flex: 1,
        backgroundColor: "#bdf9ed",
        alignItems: "center",
        justifyContent: "center",
    },
    setor2: {
        flex: 1,
        backgroundColor: "#f2f9bd",
        alignItems: "center",
        justifyContent: "center",
    },
    setor3: {
        flex: 1,
        backgroundColor: "#bdf9c4",
        alignItems: "center",
        justifyContent: "center",
    },
    setor4: {
        flex: 1,
        backgroundColor: "#fff922",
        alignItems: "center",
        justifyContent: "center",
    },
    circulo: {
        width: 100,
        height: 100,
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 50,
        backgroundColor: "red",

    },
})

export const Circulo = props => <View style={styles.circulo}></View>

export default props => {
    return (
        <View style={styles.container}>

            <View style={styles.emcima}>
                <View style={styles.setor1}>
                    <Circulo />
                </View>
                <View style={styles.setor2}>
                    <Circulo />
                </View>
            </View>
            <View style={styles.meio}>
                <Circulo />
            </View>
            <View style={styles.baixo}>
                <View style={styles.setor3}>
                    <Circulo />
                </View>
                <View style={styles.setor4}>
                    <Image source={{ uri: 'https://facebook.github.io/react/logo-og.png' }}
                        style={{ width: 100, height: 100 }} />
                </View>
            </View>
        </View>
    )
}