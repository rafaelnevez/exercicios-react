import React from 'react'
import {
    StyleSheet, View, Image, Text, TextInput, ScrollView, Button, FlatList,
    Alert
} from 'react-native'

const styles = StyleSheet.create({
    container: {
        height: '100%',
    },
    topBar: {
        flex: 1,
        height: 50,
        backgroundColor: "#efcede",
        flexDirection: "row",
    },
    logo: {
        height: 50,
        width: 50,
        marginLeft: 10,
        paddingVertical: 10,
    },
    searchBar: {
        flex: 2,
        height: 40,
        width: 40,
        backgroundColor: "white",
        borderRadius: 10,
        marginRight: 20,
        marginLeft: 10,
        marginTop: 5,
    },
    screen: {
        backgroundColor: "#f3f3f3",
        flex: 4,
        alignItems: "center",
        justifyContent: "center",

    },

    feedCard: {
        width:300,
        borderRadius: 10,
        backgroundColor: "white",
        borderWidth: 0.1,
        borderColor: "black",
        marginTop: 10,
        marginBottom: 20,

        flex: 1,
    },
    fotoFeed: {
        height: 250,
        width: "50%",
        alignItems: "center",
        justifyContent: "center",

        marginTop: 20,
        marginBottom: 20,

        borderWidth: 0.5,
        borderColor: "black",
    },
    titleFeed: {
        margin: 10,
        justifyContent: "center",


    },
    botfeed: {
        flexDirection: "row",


    },
    like: {
        flex: 1,

    },
    unlike: {
        flex: 1,

    }
})


const imgs = [
    {
        src: require("../img/homura.png"),
        title: "Homura Best Girl",
        titleColor: { color: "blue" }
    },
    {
        src: require("../img/madoka.png"),
        title: "PNC do caralho!!",
        titleColor: { color: "red" }
    },
    {
        src: require("../img/sailor.png"),
        title: "Saiiiiilorrr Lua",
        titleColor: { color: "yellow" }
    },
    {
        src: require('../img/sakura.png'),
        title: "Sakura lascadora",
        titleColor: { color: "green" }
    },
]

export const Feed = props => {
    const imagemSrc = props.src
    const title = props.title
    const notificar = msg => {
        Alert.alert("Informação", msg)
    }
    return (
        <View style={styles.feedCard}>
            <Text style={[styles.titleFeed, props.titleColor]}>{title}</Text>
            <Image source={imagemSrc} style={styles.fotoFeed} />
            <View style={styles.botfeed}>
                <View style={styles.like} >
                    <Button title="LIKE" onPress={() => notificar('LIKE!!')} />
                </View>
                <View style={{ flex: 2 }} />

                <View style={styles.unlike}>
                    <Button title="UNLIKKE" onPress={() => notificar('DESLIKE!')}  />
                </View>
            </View>
        </View>
    )

}




export default props => {

    const renderItems = ({ item }) => {
        return <Feed {...item} />
    }


    return (
        <ScrollView style={styles.container}>
            <View style={styles.topBar}>
                <Image source={require('../img/madoka_logo.png')} style={styles.logo} />
                <TextInput style={styles.searchBar} />
            </View>
            <View style={styles.screen}>
                <FlatList data={imgs} renderItem={renderItems}
                    keyExtractor={(_, index) => index.toString()} />
            </View>
        </ScrollView>
    )

}