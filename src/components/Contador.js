import React, { Component } from 'react'
import { View, Text, TouchableHighlight } from 'react-native'


export default class Contador extends Component {

    state = {
        numero: this.props.numeroInicial
    }
    //Pode iniciar o State pelo constructor ou apenas declara o objeto
   /*  constructor (props) {
       super(props)
       this.state = {...}
       this.maisUm = this.maisUm.bind(this) 
       //Serve para referenciar sempre a classe como o "this" da função,.ks
    } */

    maisUm () {
        this.setState({ numero: this.state.numero + 1 })
    }



    render() {
        return (
            <View>
                <Text style={{ fontSize: 40 }}>{this.state.numero}</Text>
                <TouchableHighlight 
                    onPress={() => this.maisUm()}
                    onLongPress={this.limpar}>
                    <Text style={{paddingLeft:50, fontSize: 40}}>Incrementar/Zerar</Text>
                </TouchableHighlight>
            </View>
        )
    }
}