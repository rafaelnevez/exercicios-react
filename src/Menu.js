import React from 'react'
import { createDrawerNavigator } from 'react-navigation'

import Simples from './components/Simples'
import Parimpar from './components/Parimpar'
import { Inverter, MegaSena } from './components/Mult'
import Contador from './components/Contador'
import Plataformas from './components/Plataformas'
import ValidarProps from './components/ValidarProps'
import Evento from './components/Evento'
import Avo from './components/ComunicacaoDireta'
import TextoSincronizado from './components/ComunicacaoIndireta'
import ListaFlex from './components/ListaFlex'
import Flex from './components/Flex'
import Facebook from './components/Facebook'


export default createDrawerNavigator({
    Facebook: {
        screen: Facebook,
    },
    Flex: {
        screen: Flex,
    },
    ListaFlex: {
        screen: ListaFlex,
        navigationOptions: { title: "Lista Flex" }
    },
    TextoSincronizado: {
        screen: TextoSincronizado,
        navigationOptions: { title: "Texto Sincronizado" }
    },
    Avo: {
        screen: () => <Avo nome="João" sobrenome="Silva" />
    },
    Evento: {
        screen: Evento
    },
    ValidarProps: {
        screen: () => <ValidarProps label={"Teste: "} ano={1} />
    },
    Plataformas: {
        screen: Plataformas
    },
    Contador: {
        screen: () => <Contador numeroInicial={100} />,
        navigationOptions: { title: "Contador" }
    },
    MegaSena: {
        screen: () => <MegaSena numeros={3} />,
        navigationOptions: { title: "Mega Sena" }
    },
    Inverter: {
        screen: () => <Inverter texto='React Nativo!' />,
        navigationOptions: { title: "Inverter" }
    },
    Parimpar: {
        screen: () => <Parimpar numero={30} />,
        navigationOptions: { title: "Parimpar" }
    },
    Simples: {
        screen: () => <Simples texto="Flexivel!!!" />,
        navigationOptions: { title: "Simples" }
    },
}, {
        drawerWidth: 300
    })
